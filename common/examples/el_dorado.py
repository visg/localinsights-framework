# Testing scripts, Please look at it once 
# Try not to change the code structure and order in your scripts
# 

import re
from bs4 import BeautifulSoup
from scraper import Scraper
from scraper.utils import get_standard_arg_parser
from datetime import datetime
import json
import sys


# ==============Write Global variables required===============
base_url = "http://main.edcgov.us/CGI/WWB012/WWM502/R?"
suffix_url = "&Q=200&S=ON#List"
output_columns = ["date", "doc_number", "document", "role", "name", "apn", "county", "state"]


# ==============Write Parset Input Values===============
# it will be consistant mostly in every file, At the end 
# you need state to continue so that parameter will only change
arguments = get_standard_arg_parser()
from_date = arguments.from_date
to_date = arguments.to_date
status = json.loads(arguments.status)


# ==============Write All the functions you need to parse the file=======
def scrape_page_data(page_url):
	while True:
		try:
			final_data = []
			with Scraper("bot", start_date="12/1/2016", end_date="12/1/2016") as s:

				response = s.requests_response(page_url)
				soup = BeautifulSoup(response.text,"html.parser")
				bottom_continue_link = soup.findAll("a", {"title" : "Continue ..."})

				if bottom_continue_link:
					all_tables = soup.findAll("table",{"class" : "NOLINE"})

					for table in all_tables:
						for tr in table.findAll('tr'):
							tds = tr.findAll('td')

							if len(tds)==3:
								document_number = tds[0].select_one("span").text
								date = tds[1].text
								names = tds[2].text.split("\xa0\xa0\xa0\xa0")
								role="Grantor"
								document_title = ""
								for i in range(0, len(names)):
									if(i==0):
										document_title = names[0]
									else:
										name = names[i]
										if(name.find("grantee")!=-1):
											name = name.split("\xa0\xa0")[0] 
											role="Grantee"
										values = [date,document_number,document_title,role,name,"Nan","El_Dorado","California"]
										final_data.append(dict(zip(output_columns,values)))
				return final_data
		except Exception as e:
			raise e


# ----------------------------------------------

# You need to make one object to run for each date
# 

# to get the current status, from where to start the bot
def get_current_status():
	from_year = datetime.strptime(from_date, '%m/%d/%Y').year
	to_year = datetime.strptime(to_date, '%m/%d/%Y').year
	doc_num = 1

	# if status is not given first time then by default it will take
	# {"status":"initial"}
	# if not initial then use given status otherwise first date and last date
	if (status["status"]!="initial"):
		from_year = int(status["status"]["year"])
		doc_num = int(status["status"]["doc_num"])

	return {"from_year":from_year,"to_year":to_year,"doc_num":doc_num}



if __name__ == "__main__":
	
	c_status = get_current_status()
	doc_num = c_status["doc_num"]
	year = c_status["from_year"]

	if(c_status["from_year"] > c_status["to_year"]):
		print({"status":"complete"})

# write the loop to run your scripts from start or according to status
# At the end get the output and pass it to run function with filename 
# ,bot_last_status and csvheaders are necessary parameters
#  Default values are there in case of not given but it is advisable to give these three
# parameters always to pass review tests

	while(year <= c_status["to_year"]):
		while (True):
			final_url = base_url + "Y="+ str(year) +"&N="+ str(doc_num) + suffix_url
			output_data = scrape_page_data(final_url)
			if not output_data:
				sys.exit(1)
				break

			filename = "{year}_{doc_num}".format(year=year,doc_num=doc_num)
			status = {"year":year,"doc_num":doc_num}

			el_dorado = Scraper("el_dorado",filename=filename, csvheaders=output_columns
																			,start_date=from_date, bot_last_status=status)
			el_dorado.save(output_data)
			doc_num+=200
		year+=1
		doc_num=1
	
